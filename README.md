Llama.vim
=========
A heavily wip project to allow interfacing with the llama.cpp example server
with vim script.

While originally conceived as an alternative to the example main.cpp that
allows for multi-line submission, editing, and regenning for chatting/instruct
without the added bloat of web browser using precious RAM, it's ease of use
seems to show promise as a general use 'magic auto-complete' button for any
text file.

## Usage
Liable to change in the future.  
Simply place llama.vim in your `~/.vim/autoload` directory
Edit the `API_URL` if needed
Place the following line, or equivalent, in your .vimrc
```vim
nnoremap Z :call llama#doLlamaGen()<CR>
```
Ensure that a working llama.cpp server is running in the background.  
Pressing Z will then begin asynchronous generation for the current buffer.
Note there will be minimal feedback for longer buffers as the prompt is processing
Generation can be stopped at any time by pressing Z again.

## Per Buffer Overrides
If the first line of the file starts with `!*` followed by a valid json string,
those arguments will merged into the default query.

For example the following override will remove all stop terms.
```
!*{"stop": []}
```
